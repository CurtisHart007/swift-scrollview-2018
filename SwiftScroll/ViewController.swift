//
//  ViewController.swift
//  SwiftScroll
//
//  Created by Carissa on 1/14/18.
//  Copyright © 2018 Carissa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scrollView = UIScrollView(frame: self.view.bounds)
        scrollView.backgroundColor = UIColor.blue
        self.view.addSubview(scrollView)
        
        
        
        
        // Sets views to be 3 x 3
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3)
        
        
        // 1st row, Left Side ScrollView
        let scrollViewer1 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer1.backgroundColor = UIColor.brown
                scrollView.addSubview(scrollViewer1)
        
        // 1st row, Middle ScrollView
        let scrollViewer2 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer2.backgroundColor = UIColor.gray
                scrollView.addSubview(scrollViewer2)
        
        // 1st row, Right Side ScrollView
        let scrollViewer3 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer3.backgroundColor = UIColor.magenta
                scrollView.addSubview(scrollViewer3)
        
        
        
        
        
        
        // 2nd row, Left Side ScrollView
        let scrollViewer4 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer4.backgroundColor = UIColor.white
                scrollView.addSubview(scrollViewer4)
        
        // 2nd row, Middle ScrollView
        let scrollViewer5 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer5.backgroundColor = UIColor.red
                scrollView.addSubview(scrollViewer5)
        
        // 2nd row, Right Side ScrollView
        let scrollViewer6 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer6.backgroundColor = UIColor.orange
                scrollView.addSubview(scrollViewer6)
        
        
        
        
        
        
        // 3rd row, Left Side ScrollView
        let scrollViewer7 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer7.backgroundColor = UIColor.yellow
                scrollView.addSubview(scrollViewer7)
        
        // 3rd row, Middle ScrollView
        let scrollViewer8 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer8.backgroundColor = UIColor.black
                scrollView.addSubview(scrollViewer8)
        
        // 3rd row, Right Side ScrollView
        let scrollViewer9 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                scrollViewer9.backgroundColor = UIColor.green
                scrollView.addSubview(scrollViewer9)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}
